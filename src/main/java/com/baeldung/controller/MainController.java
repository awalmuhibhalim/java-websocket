package com.baeldung.controller;

import com.baeldung.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping({"",""})
public class MainController {
    @Autowired
    private User user;

    @GetMapping({"userlist","userlist/"})
    public List<String> getUserList(){
        return user.getUserlist();
    }

    @GetMapping({"put","put/{username}"})
    public void setUser(@PathVariable String userName){
        user.getUserlist().add(userName);
    }
}
