package com.baeldung.model;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class User {
    public HashMap<String, String> users = new HashMap<>();

    public List<String> userlist = new ArrayList<>();

    public List<String> getUserlist() {
        return userlist;
    }

    public void setUserlist(List<String> userlist) {
        this.userlist = userlist;
    }
}
