var ws;
var user = [];
var userTarget = "all";

function connect() {
    var username = document.getElementById("username").value;
    
    var host = document.location.host;
    var pathname = document.location.pathname;
    
    ws = new WebSocket("ws://" +host  + pathname + "chat/" + username);

    ws.onmessage = function(event) {
        console.log(event.data);
        var message = JSON.parse(event.data);
        if(message.to == "all"){
            var log = document.getElementById("log");
            log.innerHTML += message.from + " : " + message.content + "\n";
        }else{
            var area = document.getElementById("log_spec_"+message.to+"");
            if(area == null){
                chatArea(message.to);
                area = document.getElementById("log_spec_"+message.to+"");
                area.innerHTML += message.from + " : " + message.content + "\n";
            }else{
                area.innerHTML += message.from + " : " + message.content + "\n";
            }
        }

        var flag = false;
        for(var i=0; i<user.length; i++){
            if(user[i] == message.from){
                flag = true;
                break;
            }
        }

        if(!flag){
            user.push(message.from)
            $('#userlist').append('<li onclick="selectUser(\''+ message.from + '\')">'+message.from+'</li>');
        }

    };
}

function selectUser(target){
    userTarget = target;
}

function chatArea(target){
    $('#table_0').append("<tr><td><textarea readonly='true' rows='10' cols='80' id='log_spec_"+target+"'></textarea></td></tr>"
                         +"<tr>"
                            +"<td>"
                                +"<input type='text' size='51' id='msg_spec_"+target+"' placeholder='Message'/>"
                                +"<button type='button' onclick='sendToPerson(\""+ target + "\")' >Send</button>"
                            +"</td>"
                        +"</tr>");
}

function send() {
    var content = document.getElementById("msg").value;
    var json = JSON.stringify({
        "content":content,
        "to":userTarget,
    });

    ws.send(json);
}

function sendToPerson(target) {
    var content = document.getElementById("msg_spec_"+target+"").value;
    var json = JSON.stringify({
        "content":content,
        "to":target,
    });

    ws.send(json);
}